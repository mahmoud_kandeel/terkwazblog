﻿using System.Collections.Generic;
using Blog.Models.DTO;

namespace Blog.Services.Interfaces.Services.Notification
{
    public interface INotificationService : IBaseService<Models.Notification, NotificationDto>
    {
        ReturnPagingDto<List<NotificationDto>> GetAll();
        ReturnModelDto<NotificationDto> GetById(int id);
        ReturnModelDto<NotificationDto> Add(NotificationDto notificationDto);
        ReturnModelDto<NotificationDto> Edit(NotificationDto notificationDto);
        ReturnModelDto<List<NotificationDto>> Delete(int id);
    }
}