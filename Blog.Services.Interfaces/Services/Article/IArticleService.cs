﻿using System.Collections.Generic;
using Blog.Models.DTO;

namespace Blog.Services.Interfaces.Services.Article
{
    public interface IArticleService : IBaseService<Models.Article, ArticleDto>
    {
        ReturnPagingDto<List<ArticleDto>> GetAll();
        ReturnModelDto<ArticleDto> GetById(int id);
        ReturnModelDto<ArticleDto> Add(ArticleDto articleDto);
        ReturnModelDto<ArticleDto> Edit(ArticleDto articleDto);
        ReturnModelDto<List<ArticleDto>> Delete(int id);
    }
}