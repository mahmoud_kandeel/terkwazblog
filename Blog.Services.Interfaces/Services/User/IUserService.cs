using Blog.Models.DTO;

namespace Blog.Services.Interfaces.Services.User
{
    public interface IUserService : IBaseService<Models.User, UserDto>
    {
        ReturnModelDto<UserDto> Authenticate(string username, string password);
        Models.User Register(string firstname, string lastname, string username, string password);
    }
}