﻿using Blog.Models;
using Blog.Repository.Interface;
using Microsoft.EntityFrameworkCore;
using System;

namespace Blog.Repository
{
    public class ServiceContext : DbContext, IServiceContext
    {
        public ServiceContext(DbContextOptions<ServiceContext> options) : base(options)
        {
        }


        #region Entites

        public DbSet<Article> Articles { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Notification> Notifications { get; set; }

        #endregion

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Article>(entity => { entity.Property(e => e.Author).IsRequired(); });

            #region ArticleSeed

            for (var i = 1; i < 100; i++)
            {
                modelBuilder.Entity<Article>().HasData(new Article
                {
                    Id = i,
                    Author = "Mahmoud Kandeel",
                    Body =
                        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                    CreationDate = DateTime.UtcNow,
                    ImageUrl = $"https://picsum.photos/id/{i}/750/300",
                    Subtitle = $"Article {i} Subtitle",
                    Title = $"Article {i} Title"
                });
            }

            #endregion

            #region UsersSeed

            modelBuilder.Entity<User>().HasData(new User
            {
                Id = 1,
                FirstName = "Mahmoud",
                LastName = "Kandeel",
                Password = "123456",
                Username = "admin"
            });

            #endregion
        }
    }
}