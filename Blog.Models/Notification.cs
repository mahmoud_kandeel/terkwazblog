﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Blog.Models
{
    public class Notification
    {
        [Key] public int Id { get; set; }
        [Required] public string Author { get; set; }
        [Required] public string Title { get; set; }
    }
}