﻿using System.Collections.Generic;

namespace Blog.Models.DTO
{
    public class ReturnPagingDto<T> where T : new()
    {
        public T Result { get; set; } = new T();
        public int PageLength { get; set; }
    }

    public class ReturnModelDto<T>
    {
        public bool Success { get; set; }
        public int Statuscode { get; set; } = 400;
        public List<Error> Errors { get; set; } = new List<Error>();
        public T Data { get; set; }
        public string SucccessMessage { get; set; }
    }

    public class Error
    {
        public string ErrorCode { get; set; }
        public string Message { get; set; }
    }
}