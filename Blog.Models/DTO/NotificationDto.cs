﻿using System.ComponentModel.DataAnnotations;

namespace Blog.Models.DTO
{
    public class NotificationDto : BaseDto<int>
    {
        [Required] public string Author { get; set; }
        [Required] public string Title { get; set; }
    }
}