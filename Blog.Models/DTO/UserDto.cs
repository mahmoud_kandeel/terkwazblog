﻿using System;

namespace Blog.Models.DTO
{
    public class UserDto : BaseDto<int>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Token { get; set; }
    }
}