using AutoMapper;
using Blog.Models.DTO;
using Blog.Repository.Interface;
using Blog.Repository.Interface.Repositories;
using Blog.Services.Interfaces.Services.User;
using Microsoft.Extensions.Configuration;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace Blog.Services.Services.User
{
    public class UserService : BaseService<Models.User, UserDto, int>, IUserService
    {
        private readonly IConfiguration _configuration;
        private readonly IRepository<Models.User> _repository;
        private readonly IMapper _map;
        private readonly IUnitOfWork _uow;


        public UserService(IRepository<Models.User> repository, IMapper map, IUnitOfWork uow, IConfiguration configuration)
            : base(repository, uow, map)
        {
            _repository = repository;
            _map = map;
            _uow = uow;
            _configuration = configuration;
        }


        public ReturnModelDto<UserDto> Authenticate(string username, string password)
        {
            var result = new ReturnModelDto<UserDto>();

            try
            {
                var user = _repository.GetAll().SingleOrDefault(x =>
                    x.Username.Trim() == username.ToLower() && x.Password.Trim() == password);

                // return null if user not found
                if (user == null)
                {
                    result.Errors.Add(new Error {Message = "User not found", ErrorCode = "400"});
                    return result;
                }

                var appSettingsSection = _configuration.GetSection("AppSettings");
                if (!appSettingsSection.Exists())
                {
                    return result;
                }

                var secretSection = appSettingsSection.GetSection("Secret");
                if (!secretSection.Exists())
                {
                    result.Errors.Add(new Error {Message = "error", ErrorCode = "400"});
                    return result;
                }

                // authentication successful so generate jwt token
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(secretSection.Value);
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                        new Claim(ClaimTypes.Name, user.Id.ToString())
                    }),
                    Expires = DateTime.UtcNow.AddDays(7),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                };
                var token = tokenHandler.CreateToken(tokenDescriptor);
                user.Token = tokenHandler.WriteToken(token);
                result.Data = new UserDto
                {
                    Id = user.Id,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Token = tokenHandler.WriteToken(token)
                };
                result.Success = true;
                result.Statuscode = 200;
                return result;
            }
            catch (Exception e)
            {
                result.Errors.Add(new Error {Message = e.Message, ErrorCode = "400"});
                return result;
            }
        }

        public Models.User Register(string firstname, string lastname, string username, string password)
        {
            var user = _repository.Create(new Models.User
            {
                FirstName = firstname,
                LastName = lastname,
                Password = password,
                Username = username
            });

            _uow.Complete();
            return user;
        }
    }
}