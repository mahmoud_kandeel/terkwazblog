﻿#region Usings

using AutoMapper;
using Blog.Models.DTO;
using Blog.Repository.Interface;
using Blog.Repository.Interface.Repositories;
using Blog.Services.Interfaces.Services.Notification;
using System;
using System.Collections.Generic;
using System.Linq;

#endregion

namespace Blog.Services.Services.Notification
{
    public class NotificationService : BaseService<Models.Notification, NotificationDto, int>, INotificationService
    {
        public NotificationService(IRepository<Models.Notification> repository, IMapper map, IUnitOfWork uow)
            : base(repository, uow, map)
        {
            _repository = repository;
            _map = map;
            _uow = uow;
        }


        private readonly IRepository<Models.Notification> _repository;
        private readonly IMapper _map;
        private readonly IUnitOfWork _uow;


        public ReturnPagingDto<List<NotificationDto>> GetAll()
        {
            var result = new ReturnPagingDto<List<NotificationDto>>();

            try
            {
                var articles = _repository.GetAll().ToList();
                var pagedData = articles.Select(article => new NotificationDto
                {
                    Id = article.Id,
                    Author = article.Author,
                    Title = article.Title
                });
                result.Result = _map.Map<List<NotificationDto>>(pagedData);
                result.PageLength = 10;
                return result;
            }
            catch (Exception ex)
            {
                return result;
            }
        }

        public ReturnModelDto<NotificationDto> GetById(int id)
        {
            var result = new ReturnModelDto<NotificationDto>();

            try
            {
                if (id <= 0)
                {
                    throw new Exception("Invalid Id");
                }

                var notificationDto = _repository.GetAll().Where(c => c.Id == id).Select(x => new NotificationDto
                {
                    Id = x.Id,
                    Author = x.Author,
                    Title = x.Title
                }).FirstOrDefault();
                result.Data = notificationDto;
                result.Statuscode = 200;
                result.Success = true;
                result.SucccessMessage = "Data fetched successfully";
            }
            catch (Exception ex)
            {
                result.Errors.Add(new Error {ErrorCode = "400", Message = ex.Message});
            }

            return result;
        }

        public ReturnModelDto<NotificationDto> Add(NotificationDto notificationDto)
        {
            var result = new ReturnModelDto<NotificationDto>();

            try
            {
                _repository.Create(new Models.Notification
                {
                    Author = notificationDto.Author,
                    Title = notificationDto.Title,
                });
                _uow.Complete();

                result.Statuscode = 200;
                result.SucccessMessage = "Data inserted successfully";
                result.Success = true;
                result.Data = notificationDto;
                return result;
            }

            catch (Exception ex)
            {
                result.Errors.Add(new Error {ErrorCode = "400", Message = ex.Message});
            }

            return result;
        }

        public ReturnModelDto<NotificationDto> Edit(NotificationDto notificationDto)
        {
            var result = new ReturnModelDto<NotificationDto> {Statuscode = 400, Success = false};

            try
            {
                if (notificationDto.Id <= 0)
                {
                    throw new Exception("Invalid Id");
                }


                var notification = _repository.GetAll().FirstOrDefault(x => x.Id == notificationDto.Id);
                if (notification is null)
                {
                    result.Success = false;
                    result.Statuscode = 404;
                    return result;
                }

                // update article
                notification.Author = notificationDto.Author;
                notification.Title = notificationDto.Title;
                _uow.Complete();
                result.Statuscode = 200;
                result.SucccessMessage = "Data updated successfully";
                result.Success = true;
                return result;
            }

            catch (Exception ex)
            {
                result.Errors.Add(new Error {ErrorCode = "400", Message = ex.Message});
            }

            return result;
        }

        public ReturnModelDto<List<NotificationDto>> Delete(int id)
        {
            var result = new ReturnModelDto<List<NotificationDto>>();

            try
            {
                if (id <= 0)
                {
                    throw new Exception("Invalid Id");
                }

                var notification = _repository.GetAll().FirstOrDefault(c => c.Id == id);
                if (notification != null)
                {
                    _repository.HardDelete(notification);
                    _uow.Complete();
                    result.Statuscode = 200;
                    result.Success = true;
                    result.SucccessMessage = "Data Deleted successfully";
                }
            }
            catch (Exception ex)
            {
                result.Errors.Add(new Error {ErrorCode = "400", Message = ex.Message});
            }

            return result;
        }
    }
}