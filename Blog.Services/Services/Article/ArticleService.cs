﻿#region Usings

using AutoMapper;
using Blog.Models.DTO;
using Blog.Repository.Interface;
using Blog.Repository.Interface.Repositories;
using Blog.Services.Interfaces.Services.Article;
using System;
using System.Collections.Generic;
using System.Linq;

#endregion

namespace Blog.Services.Services.Article
{
    public class ArticleService : BaseService<Models.Article, ArticleDto, int>, IArticleService
    {
        public ArticleService(IRepository<Blog.Models.Article> repository, IMapper map, IUnitOfWork uow)
            : base(repository, uow, map)
        {
            _repository = repository;
            _map = map;
            _uow = uow;
        }


        private readonly IRepository<Models.Article> _repository;
        private readonly IMapper _map;
        private readonly IUnitOfWork _uow;


        public ReturnPagingDto<List<ArticleDto>> GetAll()
        {
            var result = new ReturnPagingDto<List<ArticleDto>>();

            try
            {
                var articles = _repository.GetAll().ToList();
                var pagedData = articles.Select(article => new ArticleDto
                {
                    Id = article.Id,
                    Author = article.Author,
                    CreationDate = article.CreationDate,
                    Body = article.Body,
                    ImageUrl = article.ImageUrl,
                    Subtitle = article.Subtitle,
                    Title = article.Title
                });
                result.Result = _map.Map<List<ArticleDto>>(pagedData);
                result.PageLength = 10;
                return result;
            }
            catch (Exception ex)
            {
                return result;
            }
        }

        public ReturnModelDto<ArticleDto> GetById(int id)
        {
            var result = new ReturnModelDto<ArticleDto>();

            try
            {
                if (id <= 0)
                {
                    result.Errors.Add(new Error {Message = "Invalid Id", ErrorCode = "400"});
                    return result;
                }

                else
                {
                    var articleDto = _repository.GetAll().Where(c => c.Id == id).Select(x => new ArticleDto
                    {
                        Id = x.Id,
                        CreationDate = x.CreationDate,
                        Author = x.Author,
                        Body = x.Body,
                        ImageUrl = x.ImageUrl,
                        Subtitle = x.Subtitle,
                        Title = x.Title
                    }).FirstOrDefault();
                    result.Data = articleDto;
                    result.Statuscode = 200;
                    result.Success = true;
                    result.SucccessMessage = "Data fetched successfully";
                    return result;
                }
            }
            catch (Exception ex)
            {
                result.Errors.Add(new Error {ErrorCode = "400", Message = ex.Message});
                return result;
            }
        }

        public ReturnModelDto<ArticleDto> Add(ArticleDto articleDto)
        {
            var result = new ReturnModelDto<ArticleDto>();
            try
            {
                var article = _map.Map<Models.Article>(articleDto);
                article.CreationDate = DateTime.Now;
                _repository.Create(article);
                _uow.Complete();
                articleDto.Id = article.Id;
                result.Statuscode = 200;
                result.SucccessMessage = "Data inserted successfully";
                result.Success = true;
                result.Data = articleDto;
                return result;
            }

            catch (Exception ex)
            {
                result.Errors.Add(new Error {ErrorCode = "400", Message = ex.Message});
            }

            return result;
        }

        public ReturnModelDto<ArticleDto> Edit(ArticleDto articleDto)
        {
            var result = new ReturnModelDto<ArticleDto>();

            try
            {
                if (articleDto.Id <= 0)
                {
                    result.Errors.Add(new Error {Message = "Invalid Id", ErrorCode = "400"});
                    return result;
                }

                var article = _repository.GetAll().FirstOrDefault(x => x.Id == articleDto.Id);
                if (article is null)
                {
                    result.Success = false;
                    result.Statuscode = 404;
                    return result;
                }

                // update article
                article.Author = articleDto.Author;
                article.Title = articleDto.Title;
                article.Subtitle = articleDto.Subtitle;
                article.Body = articleDto.Body;
                article.CreationDate = DateTime.UtcNow;
                article.ImageUrl = articleDto.ImageUrl;
                _uow.Complete();
                result.Statuscode = 200;
                result.SucccessMessage = "Data updated successfully";
                result.Success = true;
                return result;
            }

            catch (Exception ex)
            {
                result.Errors.Add(new Error {ErrorCode = "400", Message = ex.Message});
            }

            return result;
        }

        public ReturnModelDto<List<ArticleDto>> Delete(int id)
        {
            var result = new ReturnModelDto<List<ArticleDto>>();

            try
            {
                if (id is 0)
                {
                    throw new Exception("Please Support Blog Article id");
                }

                var itemDeleted = _repository.GetAll().FirstOrDefault(c => c.Id == id);
                if (itemDeleted != null)
                {
                    _repository.HardDelete(itemDeleted);
                    _uow.Complete();
                    result.Statuscode = 200;
                    result.Success = true;
                    result.SucccessMessage = "Data Deleted successfully";
                }

                else
                {
                    throw new Exception("Blog Article Not Found or has been delete before");
                }
            }
            catch (Exception ex)
            {
                result.Errors.Add(new Error {ErrorCode = "400", Message = ex.Message});
            }

            return result;
        }
    }
}