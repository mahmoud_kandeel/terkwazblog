﻿using Blog.API.Hubs;
using Blog.Models.DTO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using System;
using Blog.Services.Interfaces.Services.Notification;

namespace Blog.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class NotificationsController : ControllerBase
    {
        private readonly IHubContext<NotificationHub, ITypedHubClient> _hubContext;
        private readonly INotificationService _notificationService;

        public NotificationsController(IHubContext<NotificationHub, ITypedHubClient> hubContext,
            INotificationService notificationService)
        {
            _hubContext = hubContext;
            _notificationService = notificationService;
        }


        [HttpGet("getall")]
        public IActionResult GetAll()
        {
            var result = _notificationService.GetAll();
            if (result.Result != null)
            {
                return Ok(result);
            }

            return BadRequest(result);
        }

        [HttpGet("getbyid/{id}")]
        public IActionResult GetById(int id)
        {
            try
            {
                var returnData = _notificationService.GetById(id);
                switch (returnData.Statuscode)
                {
                    case 200:
                        return Ok(returnData);
                    case 404:
                        return NotFound(returnData);
                    default:
                        return BadRequest(returnData);
                }
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }


        [HttpPost("add")]
        public IActionResult Add(NotificationDto articleDto)
        {
            var result = _notificationService.Add(articleDto);
            if (!result.Success)
            {
                return BadRequest(result);
            }

            _notificationService.Add(new NotificationDto
            {
                Author = articleDto.Author,
                Title = articleDto.Title
            });
            return Ok(result);
        }

        [HttpPut("edit")]
        public IActionResult Edit(NotificationDto articleDto)
        {
            var result = _notificationService.Edit(articleDto);

            if (result.Success)
            {
                return Ok(result);
            }

            return BadRequest(result);
        }

        [HttpDelete("delete")]
        public IActionResult Delete(int id)
        {
            var result = _notificationService.Delete(id);

            if (result.Success)
            {
                return Ok(result);
            }

            return BadRequest(result);
        }
    }
}