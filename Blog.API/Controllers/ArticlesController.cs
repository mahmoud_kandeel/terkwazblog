﻿using Blog.API.Hubs;
using Blog.Models.DTO;
using Blog.Services.Interfaces.Services.Article;
using Blog.Services.Interfaces.Services.Notification;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using System;

namespace Blog.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ArticlesController : ControllerBase
    {
        private readonly IArticleService _articleService;
        private readonly IHubContext<NotificationHub, ITypedHubClient> _hubContext;
        private readonly INotificationService _notificationService;

        public ArticlesController(IArticleService articleService, IHubContext<NotificationHub, ITypedHubClient> hubContext,
            INotificationService notificationService)
        {
            _articleService = articleService;
            _hubContext = hubContext;
            _notificationService = notificationService;
        }


        [HttpGet("getall")]
        public IActionResult GetAll()
        {
            var result = _articleService.GetAll();
            if (result.Result != null)
            {
                return Ok(result);
            }

            return BadRequest(result);
        }

        [HttpGet("getbyid/{id}")]
        public IActionResult GetById(int id)
        {
            var returnData = _articleService.GetById(id);
            switch (returnData.Statuscode)
            {
                case 200:
                    return Ok(returnData);
                case 404:
                    return NotFound(returnData);
                default:
                    return BadRequest(returnData);
            }
        }


        [HttpPost("add")]
        public IActionResult Add(ArticleDto articleDto)
        {
            var result = _articleService.Add(articleDto);
            if (!result.Success)
            {
                return BadRequest(result);
            }

            _notificationService.Add(new NotificationDto
            {
                Author = articleDto.Author,
                Title = articleDto.Title
            });
            _hubContext.Clients.All.NewArticle(articleDto.Title, $"Article Has been added by {result.Data.Author}", result.Data.Id, $"{articleDto.ImageUrl}");
            return Ok(result);
        }

        [HttpPut("edit")]
        public IActionResult Edit(ArticleDto articleDto)
        {
            var result = _articleService.Edit(articleDto);

            if (result.Success)
            {
                return Ok(result);
            }

            return BadRequest(result);
        }

        [HttpDelete("delete")]
        public IActionResult Delete(int id)
        {
            var result = _articleService.Delete(id);

            if (result.Success)
            {
                return Ok(result);
            }

            return BadRequest(result);
        }
    }
}