﻿using System.Threading.Tasks;

namespace Blog.API.Hubs
{
    public interface ITypedHubClient
    {
        Task NewArticle(string title, string message, int id, string image);
    }
}