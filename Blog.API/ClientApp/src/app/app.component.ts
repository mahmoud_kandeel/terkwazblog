import { Component, OnInit, Inject } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import * as signalR from '@aspnet/signalr';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {
  title = 'app';
  baseUrl = '';

  constructor(@Inject('BASE_URL') baseUrl: string, private toastr: ToastrService) {
    this.baseUrl = baseUrl;
  }

  ngOnInit(): void {
    const connection = new signalR.HubConnectionBuilder()
      .configureLogging(signalR.LogLevel.Information)
      .withUrl(`${this.baseUrl}notification`)
      .build();

    connection
      .start()
      .then(function() {
        console.log('Connected!');
      })
      .catch(function(err) {
        return console.error(err.toString());
      });

    connection.on('NewArticle', (title: string, message: string, id: string, image: string) => {
      this.showToaster(message, title, id, image);
    });
  }
  private showToaster(message: string, title: string, id: string, image) {
    this.toastr
      .success(message, title, {
        enableHtml: true,
        closeButton: true,
        timeOut: 10000
      })
      .onTap.pipe(take(1))
      .subscribe(() => this.toasterClickedHandler(id));
  }

  toasterClickedHandler(id) {
    console.log('Toastr clicked');
    window.location.href = `${this.baseUrl}articles/${id}`;
  }
}
