import { Injectable, Inject } from '@angular/core';
import { catchError, map, finalize, filter } from 'rxjs/operators';
import {
  HttpClient,
  HttpResponse,
  HttpErrorResponse,
  HttpParams,
  HttpHeaders
} from '@angular/common/http';
import { HttpHelper } from '../../../helpers/helper';

@Injectable({ providedIn: 'root' })
export class NotificationService extends HttpHelper {
  baseUrl = '';
  constructor(@Inject('BASE_URL') baseUrl: string, private http: HttpClient) {
    super();
    this.baseUrl = baseUrl;
  }

  getAll() {
    return this.http.get(`${this.baseUrl}api/notifications/getAll`).pipe(
      map((res: HttpResponse<any>) => this.onSucess(res, 'getAll')),
      catchError((error: HttpErrorResponse) => this.onError(error, 'getAll')),
      finalize(() => {
        this.onComplete('getAll');
      })
    );
  }

  getById(id: any) {
    return this.http.get(`${this.baseUrl}api/notifications/getById` + `/${id}`).pipe(
      map((res: HttpResponse<any>) => this.onSucess(res, 'getById')),
      catchError((error: HttpErrorResponse) => this.onError(error, 'getById')),
      finalize(() => {
        this.onComplete('getById');
      })
    );
  }

  add(notification: any) {
    const url = `${this.baseUrl}api/notifications/add`;
    return this.http.post(url, notification).pipe(
      map((res: HttpResponse<any>) => this.onSucess(res, 'addNotification')),
      catchError((error: HttpErrorResponse) => this.onError(error, 'addNotification')),
      finalize(() => {
        this.onComplete('addNotification');
      })
    );
  }

  edit(notification: any) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    const url = `${this.baseUrl}api/notifications/edit`;
    return this.http.put(url, notification, httpOptions).pipe(
      map((res: HttpResponse<any>) => this.onSucess(res, 'editNotification')),
      catchError((error: HttpErrorResponse) => this.onError(error, 'editNotification')),
      finalize(() => {
        this.onComplete('editNotification');
      })
    );
  }

  delete(id: any) {
    return this.http.delete(`${this.baseUrl}api/notifications/delete` + `/?id=${id}`).pipe(
      map((res: HttpResponse<any>) => this.onSucess(res, 'deleteNotification')),
      catchError((error: HttpErrorResponse) => this.onError(error, 'deleteNotification')),
      finalize(() => {
        this.onComplete('deleteNotification');
      })
    );
  }
}
