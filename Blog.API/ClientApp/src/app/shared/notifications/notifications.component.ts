import { Component, OnInit, Inject } from '@angular/core';
import { NotificationService } from './services/notififcation.service';
import * as signalR from '@aspnet/signalr';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css']
})
export class NotificationsComponent implements OnInit {
  items = [];
  pageOfItems: Array<any>;
  baseUrl = '';

  constructor(@Inject('BASE_URL') baseUrl: string, private articlesService: NotificationService) {
    this.baseUrl = baseUrl;
  }

  ngOnInit() {
    this.getall();

    const connection = new signalR.HubConnectionBuilder()
      .configureLogging(signalR.LogLevel.Information)
      .withUrl(`${this.baseUrl}notification`)
      .build();

    connection
      .start()
      .then(function() {
        console.log('Connected!');
      })
      .catch(function(err) {
        return console.error(err.toString());
      });

    connection.on('NewArticle', (title: string, message: string, id: string, image: string) => {
      this.items.push({ title, message });
    });
  }

  getall() {
    this.articlesService.getAll().subscribe((result: any) => {
      this.items = result.result;
    });
  }
}
