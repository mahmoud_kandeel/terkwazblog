import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatListModule } from '@angular/material/list';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { QuillModule } from 'ngx-quill';
import { ToastrModule } from 'ngx-toastr';
import { AppComponent } from './app.component';
import { AddArticleComponent } from './components/articles/add/add-article.component';
import { ArticlesComponent } from './components/articles/articles.component';
import { ArticleDetailsComponent } from './components/articles/details/article-details.component';
import { LoginComponent } from './components/login';
import { ErrorInterceptor, JwtInterceptor, AuthGuard } from './helpers';
import { NavMenuComponent } from './shared/nav-menu/nav-menu.component';
import { NotificationsComponent } from './shared/notifications/notifications.component';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { FooterComponent } from './shared/footer/footer.component';
import { SidebarComponent } from './shared/sidebar/sidebar.component';
import { EditArticleComponent } from './components/articles/edit/edit.component';
import { RegisterComponent } from './components/register/register.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { appRoutingModule } from './app.routing';
import { SanitizeHtmlPipe } from './helpers/sanitize-html.pipe';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    SidebarComponent,
    FooterComponent,
    ArticlesComponent,
    AddArticleComponent,
    EditArticleComponent,
    ArticleDetailsComponent,
    NotificationsComponent,
    LoginComponent,
    RegisterComponent,
    SanitizeHtmlPipe
  ],
  exports: [SanitizeHtmlPipe],
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    MatListModule,
    MatCardModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatIconModule,
    NgxPaginationModule,
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    ToastrModule.forRoot(),
    QuillModule.forRoot(),
    appRoutingModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
