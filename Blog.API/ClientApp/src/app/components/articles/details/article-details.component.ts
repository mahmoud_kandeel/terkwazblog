import { Component, OnInit } from '@angular/core';
import { ArticleService } from '../services/article.service';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-details',
  templateUrl: './article-details.component.html',
  styleUrls: ['./article-details.component.css']
})
export class ArticleDetailsComponent implements OnInit {
  id;
  article;
  constructor(
    private articleService: ArticleService,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    public _DomSanitizationService: DomSanitizer
  ) {}

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      if (params.get('id')) {
        this.id = params.get('id');
        this.loadDataById(this.id);
      }
    });
  }

  loadDataById(id: any) {
    this.articleService.getById(id).subscribe(
      (data: any) => {
        if (data.success) {
          this.article = data.data;
        }
      },
      error => {
        if (error.error.errors) {
          console.log(error);
          this.toastr.error(error.error.errors[0].message);
        } else {
          this.toastr.error('An error has occured please try again later');
        }
      }
    );
  }
}
