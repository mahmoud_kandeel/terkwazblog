import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ArticleService } from '../services/article.service';
import { first } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { AuthenticationService } from '@app/services';
import { User } from '@app/models';

@Component({
  selector: 'app-add-article',
  templateUrl: './add-article.component.html',
  styleUrls: ['./add-article.component.css']
})
export class AddArticleComponent implements OnInit {
  articleForm: FormGroup;
  submitted = false;
  currentUser: User;

  constructor(
    private formBuilder: FormBuilder,
    private articlesService: ArticleService,
    private toastr: ToastrService,
    private router: Router,
    private authenticationService: AuthenticationService
  ) {
    this.authenticationService.currentUser.subscribe(x => (this.currentUser = x));
  }

  ngOnInit() {
    this.articleForm = this.formBuilder.group({
      title: ['', Validators.required],
      subtitle: ['', Validators.required],
      imageUrl: ['https://picsum.photos/750/300', Validators.required],
      author: [`${this.currentUser.firstName} ${this.currentUser.lastName}`, Validators.required],
      body: ['', Validators.required]
    });
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.articleForm.invalid) {
      return;
    }

    this.articlesService
      .add(this.articleForm.value)
      .pipe(first())
      .subscribe(
        data => {
          this.toastr.success('New Article Added Successful', 'Success');
          this.router.navigate(['/']);
        },
        error => {
          this.toastr.error(error);
          this.submitted = false;
        }
      );
  }

  onReset() {
    this.submitted = false;
    this.articleForm.reset();
  }
}
