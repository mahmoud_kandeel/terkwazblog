import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ArticleService } from '../services/article.service';
import { ToastrService } from 'ngx-toastr';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-edit-article',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditArticleComponent implements OnInit {
  articleForm: FormGroup;
  submitted = false;
  blured = false;
  focused = false;
  id: number;
  constructor(
    private formBuilder: FormBuilder,
    private articleService: ArticleService,
    private toastr: ToastrService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.articleForm = this.formBuilder.group({
      title: ['', Validators.required],
      subtitle: ['', Validators.required],
      imageUrl: ['', Validators.required],
      author: ['', Validators.required],
      body: ['', Validators.required]
    });
    this.route.paramMap.subscribe(params => {
      if (params.get('id')) {
        this.id = parseInt(params.get('id'), 10);
        this.loadDataById(this.id);
      }
    });
  }

  loadDataById(id: any) {
    this.articleService.getById(id).subscribe(
      (res: any) => {
        if (res.success) {
          this.articleForm.patchValue(res.data);
          this.articleForm.markAsTouched();
          this.articleForm.markAsDirty();
        }
      },
      error => {
        if (error.error.errors) {
          console.log(error);
          this.toastr.error(error.error.errors[0].message);
        } else {
          this.toastr.error('An error has occured please try again later');
        }
      }
    );
  }

  created(event) {
    console.log('editor-created', event);
  }

  changedEditor(event) {
    console.log('editor-change', event);
  }

  focus($event) {
    console.log('focus', $event);
    this.focused = true;
    this.blured = false;
  }

  blur($event) {
    console.log('blur', $event);
    this.focused = false;
    this.blured = true;
  }

  onChange($event: any): void {
    console.log('onChange');
  }

  onPaste($event: any): void {
    console.log('onPaste');
  }

  onSubmit() {
    this.submitted = true;
    if (this.articleForm.invalid) {
      return;
    }
    const article = this.articleForm.value;
    article.id = this.id;
    this.articleService
      .edit(this.articleForm.value)
      .pipe(first())
      .subscribe(
        res => {
          this.toastr.success('Success', 'Success');
          this.router.navigate(['/']);
        },
        error => {
          this.toastr.error(error);
          this.submitted = false;
        }
      );
  }

  onReset() {
    this.submitted = false;
    this.articleForm.reset();
  }
}
