import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ArticleService } from './services/article.service';
import { ToastrService } from 'ngx-toastr';
import { DomSanitizer } from '@angular/platform-browser';
import { AuthenticationService } from '@app/services';
import { User } from '@app/models';

@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.css']
})
export class ArticlesComponent implements OnInit {
  items = [];
  pageOfItems: Array<any>;
  currentUser: User;

  constructor(
    private articlesService: ArticleService,
    private toastr: ToastrService,
    public _DomSanitizationService: DomSanitizer,
    private authenticationService: AuthenticationService
  ) {
    this.authenticationService.currentUser.subscribe(x => (this.currentUser = x));
  }

  ngOnInit() {
    this.getall();
  }

  onChangePage(pageOfItems: Array<any>) {
    // update current page of items
    this.pageOfItems = pageOfItems;
  }

  getall() {
    this.articlesService.getAll().subscribe((result: any) => {
      this.items = result.result;
    });
  }

  delete(id: any) {
    if (window.confirm('Are sure you want to delete this item ?')) {
      this.articlesService.delete(id).subscribe(
        (result: any) => {
          this.toastr.success(result.succcessMessage);
          this.getall();
        },
        (error: any) => {
          this.toastr.error(error.error.errors[0].message);
        },
        () => {}
      );
    }
  }
}
