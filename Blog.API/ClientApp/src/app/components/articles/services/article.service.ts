import { Injectable, Inject } from '@angular/core';
import { Article } from '../../../models/article';
import { catchError, map, finalize, filter } from 'rxjs/operators';
import { HttpClient, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { HttpHelper } from '../../../helpers/helper';

@Injectable({ providedIn: 'root' })
export class ArticleService extends HttpHelper {
  baseUrl = '';
  constructor(@Inject('BASE_URL') baseUrl: string, private http: HttpClient) {
    super();
    this.baseUrl = baseUrl;
  }

  getAll() {
    return this.http.get(`${this.baseUrl}api/articles/getAll`).pipe(
      map((res: HttpResponse<any>) => this.onSucess(res, 'getAll')),
      catchError((error: HttpErrorResponse) => this.onError(error, 'getAll')),
      finalize(() => {
        this.onComplete('getAll');
      })
    );
  }

  getById(id: number) {
    return this.http.get(`${this.baseUrl}api/articles/getById` + `/${id}`).pipe(
      map((res: HttpResponse<any>) => this.onSucess(res, 'getById')),
      catchError((error: HttpErrorResponse) => this.onError(error, 'getById')),
      finalize(() => {
        this.onComplete('getById');
      })
    );
  }

  add(article: Article) {
    const url = `${this.baseUrl}api/articles/add`;
    return this.http.post(url, article).pipe(
      map((res: HttpResponse<any>) => this.onSucess(res, 'addArticle')),
      catchError((error: HttpErrorResponse) => this.onError(error, 'addArticle')),
      finalize(() => {
        this.onComplete('addArticle');
      })
    );
  }

  edit(article: Article) {
    const url = `${this.baseUrl}api/articles/edit`;
    return this.http.put(url, article).pipe(
      map((res: HttpResponse<any>) => this.onSucess(res, 'addArticle')),
      catchError((error: HttpErrorResponse) => this.onError(error, 'addArticle')),
      finalize(() => {
        this.onComplete('addArticle');
      })
    );
  }

  delete(id: any) {
    return this.http.delete(`${this.baseUrl}api/articles/delete` + `/?id=${id}`).pipe(
      map((res: HttpResponse<any>) => this.onSucess(res, 'deleteArticle')),
      catchError((error: HttpErrorResponse) => this.onError(error, 'deleteArticle')),
      finalize(() => {
        this.onComplete('deleteArticle');
      })
    );
  }
}
