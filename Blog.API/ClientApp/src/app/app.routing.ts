import { Routes, RouterModule } from '@angular/router';
import { ArticlesComponent } from './components/articles/articles.component';
import { LoginComponent } from './components/login';
import { RegisterComponent } from './components/register/register.component';
import { NotificationsComponent } from './shared/notifications/notifications.component';
import { AuthGuard } from './helpers';
import { AddArticleComponent } from './components/articles/add/add-article.component';
import { EditArticleComponent } from './components/articles/edit/edit.component';
import { ArticleDetailsComponent } from './components/articles/details/article-details.component';

const routes: Routes = [
  { path: '', component: ArticlesComponent, pathMatch: 'full' },
  { path: 'login', component: LoginComponent, pathMatch: 'full' },
  { path: 'register', component: RegisterComponent, pathMatch: 'full' },
  {
    path: 'notifications',
    component: NotificationsComponent,
    pathMatch: 'full',
    canActivate: [AuthGuard]
  },
  {
    path: 'articles',
    component: ArticlesComponent,
    pathMatch: 'full'
  },
  {
    path: 'articles/add',
    component: AddArticleComponent,
    pathMatch: 'full',
    canActivate: [AuthGuard]
  },
  {
    path: 'articles/edit/:id',
    component: EditArticleComponent,
    pathMatch: 'full',
    canActivate: [AuthGuard]
  },
  {
    path: 'articles/:id',
    component: ArticleDetailsComponent,
    pathMatch: 'full'
  },
  { path: '**', redirectTo: '' }
];

export const appRoutingModule = RouterModule.forRoot(routes);
