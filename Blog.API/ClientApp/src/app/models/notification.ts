﻿export class Notification {
  id: number;
  title: string;
  author: string;
}
