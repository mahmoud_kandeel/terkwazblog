export class Article {
  id: string;
  title: string;
  subTitle: string;
  body: string;
  author: string;
  imageUrl: string;
}
