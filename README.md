# For Backend

- in backage manger console set default project "Data\Blog.Repository"
- run Update-Database to create db and seed data
  ![Screenshot](https://i.ibb.co/yyx5mPn/Annotation-2019-11-09-082735.png)

- default username admin password 123456

# For Angular

![Screenshot](https://i.ibb.co/GCDgXNb/Annotation-2019-11-09-082504.png)

![Screenshot](https://i.ibb.co/R0FZ9TJ/image.png)

![Screenshot](https://i.ibb.co/ZJz2xxP/image.png)
